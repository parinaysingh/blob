import axios from 'axios';

export default {
    user: {
        login: ( token ) => axios.post('/auth', { token }).then(res => res.data),
        fetch: () => axios.get('/auth/fetch').then(res => res.data.user)
    },
    posts: {
        fetchAll: () => axios.get('/posts').then(res => res.data.posts),
        create: data =>
            axios.post('/posts', data).then(res => res.data.post),
        remove: pid => axios.get(`/posts/remove/${pid}`).then(res => res.data)
    },
    comments: {
        fetch: pid => axios.get(`/contribs/${pid}`).then(res => res.data.contribs),
        create: data =>
            axios.post('/contribs', data).then(res => res.data.contribs)
    }
};
