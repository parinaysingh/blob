import {combineReducers} from 'redux';

import {POSTS_FETCHED, POST_CREATED, POST_REMOVED} from '../types';

const posts = (state = {}, action) => {
    switch (action.type) {
        case POSTS_FETCHED:
        case POST_CREATED:
            return {...state, ...action.data.entities.posts};
        case POST_REMOVED:
            let copy = Object.assign({}, state);
            delete copy[action.data];
            return copy;
        default:
            return state;
    }
}

const authors = (state = {}, action) => {
    switch (action.type) {
        case POSTS_FETCHED:
        case POST_CREATED:
            return {...state, ...action.data.entities.authors};
        default:
            return state;
    }
}

export default combineReducers({
    posts,
    authors
});