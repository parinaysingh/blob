import { schema, Array } from 'normalizr';

export const authorSchema = new schema.Entity('authors', {},
    {idAttribute: '_id'}
);

export const postSchema = new schema.Entity('posts', {
    author: authorSchema,
    },
  { idAttribute: '_id' }
);
