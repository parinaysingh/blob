import {normalize} from 'normalizr';
import {POSTS_FETCHED, POST_CREATED, POST_REMOVED} from '../types';
import api from '../api';
import {postSchema} from '../schemas';

const postsFetched = data => ({
    type: POSTS_FETCHED,
    data
});

const postCreated = data => ({
    type: POST_CREATED,
    data
});

const postRemoved = data => ({
    type: POST_REMOVED,
    data
});

export const fetchPosts = () => dispatch =>
    api.posts
        .fetchAll()
        .then(posts => dispatch(postsFetched(normalize(posts.reverse(), [postSchema]))));

export const createPosts = ({authUser, data }) => dispatch =>
    api.posts
        .create(data)
        .then((post) => {
            post.author = {
                _id: authUser._id,
                firstname: authUser.firstname,
                lastname: authUser.lastname,
                profilephoto: authUser.profilephoto
            }
            console.log(post);
            dispatch(postCreated(normalize(post, postSchema)));
            return ('done');
        });

export const removePosts = pid => dispatch =>
    api.posts
        .remove(pid)
        .then(success => dispatch(postRemoved(pid)));