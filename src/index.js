import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './rootReducer';
import { userLoggedIn } from './actions/auth';
import setAuthorizationHeader from './utils/setAuthorizationHeader';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import api from './api';

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunk))
);

if (localStorage.webJWT) {
    const token = localStorage.webJWT;
    setAuthorizationHeader(token);
    store.dispatch(userLoggedIn({ token }));
    api.user.fetch().then((userData) => {
        store.dispatch(userLoggedIn({
            user: userData,
            token
        }));
    })
}

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <Route component={App} />
        </Provider>
    </BrowserRouter>, document.getElementById('root'));
registerServiceWorker();

