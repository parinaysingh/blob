import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {logout} from '../actions/auth';

class Header extends React.Component {
    render() {
        return (
            <header id="header_main">
                <div id="logo"><h2>BLOB!</h2></div>
                <div className="account"><img
                    src="https://lh3.googleusercontent.com/-1CC17DX9om4/AAAAAAAAAAI/AAAAAAAAAAA/ACnBePYOolya_hCbngHPZtsPohd2eV9wfA/s64-c-mo/photo.jpg"
                    alt=""/>
                    <div className="options uparrowdiv" style={{color: '#222'}}>
                        <a href="#">Profile</a>
                        <a href="#" onClick={this.props.logout}>Logout</a>
                    </div>
                </div>
            </header>
        );
    }
}

Header.propTypes = {
    logout: PropTypes.func.isRequired
};

export default connect(null, {logout})(Header);
