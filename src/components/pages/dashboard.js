import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import ArticleQuestion from './dashboard/ArticleQuestion';
import ArticleImage from './dashboard/ArticleImage';
import ArticleLink from './dashboard/ArticleLink';
import {fetchPosts} from '../../actions/posts';
import '../../css/dashboard.css';
import Header from '../header';
import Footer from '../footer';
import Toolbar from '../toolbar';
import Sidebar from '../sidebar';
import Editor from '../Editor';

class dashboard extends React.Component {
    componentDidMount = () => this.onInit(this.props);
    onInit = props => props.fetchPosts().catch(err => {
        console.log(err);
    });

    renderPost(post) {
        const {authors} = this.props;
        if (post.type === 'question') {
            return (
                <ArticleQuestion post={post} author={authors[post.author]}/>
            );
        } else if (post.type === 'file') {
            return (
                <ArticleImage post={post} author={authors[post.author]}/>
            );
        } else if (post.type === 'link') {
            return (
                <ArticleLink post={post} author={authors[post.author]}/>
            );
        }
    }

    render() {
        const {posts} = this.props;
        return (
            <div>
                <Header/>
                <div className="pagewrapper">
                    <Toolbar/>
                    <section className="content">
                        <Editor/>
                        <section className="content_main">
                            {Object.keys(posts).reverse().map((key) => {
                                return (
                                    <div key={key}>
                                        {this.renderPost(posts[key])}
                                    </div>
                                );
                            })}
                        </section>
                    </section>
                    <Sidebar/>
                    <div className="clear"/>
                </div>
                <Footer/>
            </div>
        );
    }
}

dashboard.propTypes = {
    posts: PropTypes.object.isRequired,
    authors: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        posts: state.posts.posts,
        authors: state.posts.authors,
    }
}

export default connect(mapStateToProps, {fetchPosts})(dashboard);
