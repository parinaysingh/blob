import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {allPostsSelector} from '../../reducers/posts';
import {fetchPosts} from '../../actions/posts';
import '../../css/dashboard.css';
import Header from '../header';
import Footer from '../footer';
import Toolbar from '../toolbar';
import Sidebar from '../sidebar';
import Editor from '../Editor';

class dashboard extends React.Component {
    componentDidMount = () => this.onInit(this.props);
    onInit = props => props.fetchPosts().catch(err => {
        console.log(err);
    });

    renderPost(post) {
        if (post['type'] === 'question') {
            return (
                <ArticleQuestion post={post}/>
            );
        } else if (post['type'] === 'file') {
            return (
                <ArticleImage post={post}/>
            );
        } else if (post['type'] === 'link') {
            return (
                <ArticleLink post={post}/>
            );
        }
    }

    render() {
        const {posts} = this.props;
        return (
            <div>
                <Header/>
                <div className="pagewrapper">
                    <Toolbar/>
                    <section className="content">
                        <Editor/>
                        <section className="content_main">
                            {
                                Object.keys(posts).reverse().map((key) => {
                                    return (
                                        <div key={key}>
                                            {this.renderPost(posts[key])}
                                        </div>
                                    );
                                })
                            }
                        </section>
                    </section>
                    <Sidebar/>
                    <div className="clear"/>
                </div>
                <Footer/>
            </div>
        );
    }
}

const ArticleQuestion = ({post}) => (
    <div>
        <article className="article_question">
            <ArticleHeader user={post['user']} postedIn={post['postedIn']}/>
            <div className="post_content">
                <div className="description">
                    <h3>{ post['post']['question']}</h3>
                    <small>{post['post']['description']}</small>
                </div>
            </div>
            <ArticleFooter upvotes={post['upvotes']} contribs={post['contribs']}/>
        </article>
        <ArticleComments/>
    </div>
);

const ArticleImage = ({post}) => (
    <div>
        <article className="article_image">
            <ArticleHeader user={post['user']} postedIn={post['postedIn']}/>
            <div className="post_content">
                <div className="post_image">
                    <img src={post['post']['file']} alt=""/>
                </div>
                <small>{post['post']['description']}</small>
            </div>
            <ArticleFooter upvotes={post['upvotes']} contribs={post['contribs']}/>
        </article>
        <ArticleComments/>
    </div>
);

const ArticleLink = ({post}) => (
    <div>
        <article className="article_link">
            <ArticleHeader user={post['user']} postedIn={post['postedIn']}/>
            <div className="post_content">
                <div className="description">{post['post']['description']} - <a href={props.link}>{props.link}</a></div>
                <div className="post_wrapper">
                    <img className="post_image" src={require('../../images/webdesignforagencies.png')} alt=""/>
                    <div className="urlPreview">
                        <span>What UX Designers can learn from gaming</span>
                        <small>{post['post']['link']}</small>
                    </div>
                </div>
            </div>
            <ArticleFooter upvotes={post['upvotes']} contribs={post['contribs']}/>
        </article>
        <ArticleComments/>
    </div>

);

const ArticleComments = () => (
    <div className="article_comment">
        <div className="new_comment">
            <textarea type="text" name="comment" placeholder="Add a contribution"/> <img
            src={require('../../images/icons/submit.svg')} alt=""/>
        </div>
        <div className="all_comments">
            <div className="comment">
                <img className="author_image"
                     src="https://lh3.googleusercontent.com/-1CC17DX9om4/AAAAAAAAAAI/AAAAAAAAAAA/ACnBePYOolya_hCbngHPZtsPohd2eV9wfA/s64-c-mo/photo.jpg"/>
                <div className="comment_content">
                    <img className="options" src={require('../../images/icons/options.svg')} alt=""/>
                    <strong className="author_name">Nishant Rai</strong>
                    <small>2 min</small>
                    <span className="comment_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it.</span>
                </div>
            </div>
        </div>
    </div>
);

const ArticleHeader = ({postedIn, user}) => (
    <header>
        <div className="author">
            <img
                className="author_image"
                src={user.profilephoto}
                alt=""
            />
            <div>
                <span className="author_name"><strong>{user.firstname} {user.lastname}</strong></span>
                <small className="posted_in">posted in #{postedIn}</small>
            </div>
        </div>
        <div className="post_time">
            <small>1 min</small>
            <img src={require('../../images/icons/options.svg')} alt=""/>
        </div>
    </header>
);

const ArticleFooter = ({upvotes, contribs}) => (
    <footer>
        <img src={require('../../images/icons/icupvote.svg')} alt=""/><span>{upvotes} Upvotes </span>
        <img src={require('../../images/icons/iccontribution.svg')} alt=""/> <span>{contribs} Contributions </span>
    </footer>
);

ArticleQuestion.prototypes = {
    post: PropTypes.array.isRequired
};

ArticleImage.prototypes = {
    post: PropTypes.array.isRequired
};

ArticleLink.prototypes = {
    post: PropTypes.array.isRequired
};

ArticleHeader.prototypes = {
    postedIn: PropTypes.string.isRequired,
    user: PropTypes.array.isRequired
};

ArticleFooter.prototypes = {
    upvotes: PropTypes.string.isRequired,
    contribs: PropTypes.string.isRequired,
};

dashboard.propTypes = {
    fetchPosts: PropTypes.func.isRequired,
    posts: PropTypes.arrayOf(
        PropTypes.shape({
            contribs: PropTypes.number.isRequired,
            post: PropTypes.object.isRequired,
            postedBy: PropTypes.string.isRequired,
            postedIn: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired,
            upvotes: PropTypes.number.isRequired,
            _id: PropTypes.string.isRequired,
        }).isRequired
    ).isRequired
};

function mapStateToProps(state) {
    return {
        posts: allPostsSelector(state)
    };
}

export default connect(mapStateToProps, {fetchPosts})(dashboard);
