import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login, logout } from '../../actions/auth';

class homePage extends React.Component {
    state = {
        errors: {},
        loading: false
    };

    componentDidMount() {
        function loadScript() {
            const script = document.createElement('script');
            script.src = 'https://apis.google.com/js/platform.js';
            document.head.appendChild(script);
        }
        loadScript();
    }

    googleLogin = () => {
        window.gapi.load('auth2', () => {
            const auth2 = window.gapi.auth2.init({
                client_id: '692050302223-522argi1vrkpt38vgpj2v2ksk0jtism1.apps.googleusercontent.com',
                scope: '',
                response_type: 'id_token'
            });
            auth2.signIn().then(() => {
                this.setState({ loading: true });
                this.props.login(auth2.currentUser.Ab.Zi.id_token).then((err) => {
                    if (err) {
                        this.setState({ errors: { msg: err }, loading: false });
                    } else {
                        this.props.history.push('/dashboard');
                    }
                });
            });
        });
    };

    logout = () => {
        this.props.logout();
    };

    render() {
        const { errors, loading } = this.state;
        return (
            <div>
                {errors.msg && <h1>{errors.msg}</h1>}
                {loading && <h1>Loading...</h1>}
                {this.props.isAuthenticated ?
                    <button onClick={this.logout}>Log Out</button> :
                    <button onClick={this.googleLogin}>Login</button>}
            </div>
        );
    }
}

homePage.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired
    }).isRequired,
    login: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired
};
function mapStateToProps(state) {
    return {
        isAuthenticated: !!state.user.token
    };
}

export default connect(mapStateToProps, { login, logout })(homePage);
