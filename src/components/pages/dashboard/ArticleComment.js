import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import api from '../../../api';

class ArticleComment extends React.Component {
    state = {
        newComment: '',
        comments: [],
        loadButton: true,
    }
    onSubmit = () => {
        api.comments.create({comment: this.state.newComment, pid: this.props.pid}).then((data) => {
            const {authUser} = this.props;
            const comment = {
                _id: data._id,
                comment: data.comment,
                author: {
                    _id: authUser._id,
                    firstname: authUser.firstname,
                    lastname: authUser.lastname,
                    profilephoto: authUser.profilephoto
                }
            }
            this.setState({...this.state, newComment: '', comments: this.state.comments.concat(comment)});

        })
    }

    loadComments = () => {
        api.comments.fetch(this.props.pid).then((comments) => {
            comments.forEach((comment) => {
                this.setState({...this.state, comments: this.state.comments.concat(comment), loadButton: false});
            })
        })
    }

    render() {
        const {comments} = this.state;
        return (
            <div className="article_comment">
                <div className="new_comment">
                    <textarea onChange={e => this.setState({...this.state, newComment: e.target.value})}
                              value={this.state.newComment} name="comment" placeholder="Add a contribution"/>
                    <img src={require('../../../images/icons/submit.svg')} onClick={this.onSubmit} alt=""/>
                </div>
                {this.state.loadButton &&
                <button onClick={this.loadComments} style={{margin: 'auto', display: 'block'}}>Load Comments</button>}
                <div className="all_comments">
                    {Object.keys(comments).reverse().map((key) => {
                        return (
                            <div className="comment" key={key}>
                                <img className="author_image"
                                     src={comments[key].author.profilephoto}/>
                                <div className="comment_content">
                                    <strong className="author_name">{comments[key].author.firstname}</strong>
                                    <img className="options" src={require('../../../images/icons/options.svg')} alt=""/>
                                    <small>2 min</small>
                                    <span className="comment_text">{comments[key].comment}</span>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}

ArticleComment.propTypes = {
    pid: PropTypes.string.isRequired,
    authUser: PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    return {
        authUser: state.user.user
    }
}

export default connect(mapStateToProps, {})(ArticleComment);