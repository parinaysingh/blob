import React from 'react';
import ArticleHeader from './ArticleHeader';
import ArticleComment from './ArticleComment';
import ArticleFooter from './ArticleFooter';

class ArticleImage extends React.Component {
    state = {
        dispComments: false
    };
    toggleComments = e => {
        if (this.state.dispComments === false) {
            this.setState({dispComments: true});
        } else {
            this.setState({dispComments: false});
        }
    };

    render() {
        const {post, author} = this.props;
        return (
            <div>
                <article className="article_image">
                    <ArticleHeader pid = {post._id} author={author} postedIn={post.postedIn}/>
                    <div className="post_content">
                        <div className="post_image">
                            <img src={post.post.file} alt=""/>
                        </div>
                        <small>{post.post.description}</small>
                    </div>
                    <ArticleFooter upvotes={post.upvotes} contribs={post.contribs}
                                   toggleComments={this.toggleComments}/>
                </article>
                {this.state.dispComments && <ArticleComment pid={post._id}/>}
            </div>
        );
    }
}

export default ArticleImage;