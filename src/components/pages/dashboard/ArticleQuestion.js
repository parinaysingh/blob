import React from 'react';
import ArticleHeader from './ArticleHeader';
import ArticleComment from './ArticleComment';
import ArticleFooter from './ArticleFooter';

class ArticleQuestion extends React.Component {
    state = {
        dispComments: false
    };
    toggleComments = e => {
        if (this.state.dispComments === false) {
            this.setState({dispComments: true});
        } else {
            this.setState({dispComments: false});
        }
    };

    render() {
        const {post, author} = this.props;
        return (
            <div>
                <article className="article_question">
                    <ArticleHeader pid = {post._id} author={author} postedIn={post.postedIn}/>
                    <div className="post_content">
                        <div className="description">
                            <h3>{post.post.question}</h3>
                            <small>{post.post.description}</small>
                        </div>
                    </div>
                    <ArticleFooter upvotes={post.upvotes} contribs={post.contribs}
                                   toggleComments={this.toggleComments}/>
                </article>
                {this.state.dispComments && <ArticleComment pid={post._id}/>}
            </div>
        );
    }
}

export default ArticleQuestion;