import React from 'react';
import ArticleHeader from './ArticleHeader';
import ArticleComment from './ArticleComment';
import ArticleFooter from './ArticleFooter';

class ArticleLink extends React.Component {
    state = {
        dispComments: false
    };
    toggleComments = e => {
        if (this.state.dispComments === false) {
            this.setState({dispComments: true});
        } else {
            this.setState({dispComments: false});
        }
    };

    render() {
        const {post, author} = this.props;
        return (
            <div>
                <article className="article_link">
                    <ArticleHeader pid = {post._id} author={author} postedIn={post.postedIn}/>
                    <div className="post_content">
                        <div className="description">{post.post.description} -
                            <a href={post.post.link} target='_blank'>{post.post.link}</a>
                        </div>
                        <div className="post_wrapper">
                            <img className="post_image" src={require('../../../images/webdesignforagencies.png')} alt=""/>
                            <div className="urlPreview">
                                <span>What UX Designers can learn from gaming</span>
                                <small>{post.post.link}</small>
                            </div>
                        </div>
                    </div>
                    <ArticleFooter upvotes={post.upvotes} contribs={post.contribs}
                                   toggleComments={this.toggleComments}/>
                </article>
                {this.state.dispComments && <ArticleComment pid={post._id}/>}
            </div>
        );
    }
}

export default ArticleLink;