import React from 'react';
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import {removePosts} from "../../../actions/posts";
import api from '../../../api';

class ArticleHeader extends React.Component {
    state = {
        dispOptions: ''
    }

    toggleDispOptions = () => {
        if(this.state.dispOptions === ''){
            this.setState({dispOptions: 'dispOptions'})
        }else{
            this.setState({dispOptions: ''})
        }
    }

    removeArticle = e => {
        this.props.removePosts(this.props.pid);
    }

    render() {
        const { postedIn, author, authUserId} = this.props;
        const { dispOptions } = this.state;
        const authUserArticle = authUserId === author._id;
        return (
            <header>
                <div className="author">
                    <img
                        className="author_image"
                        src={author.profilephoto}
                        alt=""
                    />
                    <div>
                        <span className="author_name"><strong>{author.firstname}</strong></span>
                        <small className="posted_in">posted in #{postedIn}</small>
                    </div>
                </div>
                <div className="post_time">
                    <small>1 min</small>
                    <img className={dispOptions} onClick={this.toggleDispOptions} src={require('../../../images/icons/options.svg')} alt=""/>
                    <div className={`options ${dispOptions}`}>
                        {!authUserArticle && <div>Block User</div>}
                        {authUserArticle && <div onClick={this.removeArticle}>Delete...</div>}
                    </div>
                </div>
            </header>
        );
    }
}

ArticleHeader.propTypes = {
    postedIn: PropTypes.string.isRequired,
    author: PropTypes.object.isRequired,
    authUserId: PropTypes.string.isRequired
}

function mapStateToProps(state) {
    return {
        authUserId: state.user.user._id
    }
}

export default connect(mapStateToProps, {removePosts})(ArticleHeader);