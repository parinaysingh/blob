import React from 'react';


class ArticleFooter extends React.Component {
    state = {
        upvoteState: 'active'
    }

    render() {
        const {upvotes, contribs, toggleComments} = this.props;
        const {upvoteState} = this.state;
        return (
            <footer>
                <img src={require('../../../images/icons/icupvote.svg')} alt="" className={upvoteState}/><span
                className={upvoteState}>{upvotes} Upvotes </span>
                <img src={require('../../../images/icons/iccontribution.svg')} alt=""/> <span
                onClick={toggleComments}>{contribs} Contributions </span>
            </footer>
        );
    }
}

export default ArticleFooter;