import React, { Component } from 'react';

class Toolbar extends Component{
    render(){
        return(
            <section className="toolbar">
                <div className="toolbar_header">
                    <h3>Nishant Rai <sup className="badge"></sup> </h3>
                    <small>2001 karma</small>
                </div>
                <div className="toolbar_post">
                    <img src={require("../images/icons/icadd.svg")} alt=""/>
                    <span> Add a new Topic?</span>
                </div>
                <div className="toolbar_content">
                    <a href="#">#LearnANewThing</a>
                    <a href="#">#AskASenior</a>
                    <a href="#">#WhatToLearn?</a>
                    <a href="#">#LearnANewThing</a>
                    <a href="#">#AskASenior</a>
                    <a href="#">#WhatToLearn?</a>
                    <a href="#">#LearnANewThing</a>
                    <a href="#">#AskASenior</a>
                    <a href="#">#WhatToLearn?</a>
                </div>
            </section>
        );
    }
}

export default Toolbar;
