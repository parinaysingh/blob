import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {createPosts} from '../actions/posts';

class Editor extends React.Component {
    state = {
        data: {
            type: 'question',
            question: '',
            file: '',
            link: '',
            description: '',
            postedIn: ''
        },
        errors: {
            question: '',
            file: '',
            link: '',
            postedIn: '',
            description: ''
        },
        question: 'active',
        file: '',
        link: '',
        editor: false,
        loading: false,
        imagePreviewUrl: ''
    };

    onChange = e => this.setState({
        ...this.state, data: {...this.state.data, [e.target.name]: e.target.value}
    });

    onImageChange = e => {
        e.preventDefault();
        const reader = new FileReader();
        const file = e.target.files[0];
        reader.onloadend = () => {
            this.setState({
                ...this.state,
                data: {...this.state.data, file},
                imagePreviewUrl: reader.result
            });
        };
        reader.readAsDataURL(file);
    };

    onSubmit = e => {
        e.preventDefault();
        const {data} = this.state;
        const errors = this.validate(data);
        this.setState({errors});
        if (Object.keys(errors).length === 0) {
            this.setState({loading: true});
            this.props.createPosts(this.cleanData(data))
                .then(msg => {
                    this.setState({loading: false, editor: false});
                    console.log(msg);
                })
                .catch(err => {
                    this.setState({loading: false});
                    console.log("Error");
                });
        }
    };

    validate = (data) => {
        const errors = {};
        if (data.type === 'question') {
            if (!data.question) errors.question = 'error';
        } else if (data.type === 'file') {
            if (!data.file) errors.file = 'error';
        } else if (data.type === 'link') {
            if (!data.link) errors.link = 'error';
        }
        if (!data.postedIn) errors.postedIn = 'error';
        if (!data.description) errors.description = 'error';
        return errors;
    };

    cleanData = (data) => {
        let post;
        if (data.type === 'question') {
            post = {
                question: data.question,
                description: data.description
            };
        } else if (data.type === 'file') {
            post = {
                file: data.file,
                description: data.description
            };
        } else if (data.type === 'link') {
            post = {
                link: data.link,
                description: data.description
            };
        }
        return {
            type: data.type,
            post,
            postedIn: data.postedIn
        };
    };

    render() {
        const {data, imagePreviewUrl, question, file, link, editor, errors, loading} = this.state;
        return (
            <div>
                <div id='btn-show-modal'
                     className={editor ? 'hidden' : ''}
                     onClick={() => this.setState({editor: true})}>
                    <img src={require('../images/icons/edit.svg')} alt=""/>
                </div>
                <form id='modal' onSubmit={this.onSubmit} className={editor ? '' : 'hidden'} method='post' encType='multipart/form-data'>
                    <header>
                        <h3>Editor</h3>
                        <img src={require('../images/icons/iccancel.svg')}
                             onClick={() => this.setState({editor: false})}/>
                    </header>
                    <div className='user_input'>
                        <div className={`primary_input ${question} ${errors.question}`}>
                            <input type='text' name='question' value={data.question} onChange={this.onChange}
                                   placeholder='Question?'/>
                        </div>
                        <div className={`primary_input ${file} ${errors.file}`}>
                            <label htmlFor='file' id='file_label'><span className='btn_upload'>Choose a file... </span></label>
                            <input type='file' name='file' onChange={this.onImageChange} id='file'/>
                            {imagePreviewUrl && <div className="imgPreview"><img src={imagePreviewUrl}/></div>}
                        </div>
                        <div className={`primary_input ${link} ${errors.link}`}>
                            <input type='url' name='link' value={data.link} onChange={this.onChange}
                                   placeholder='Link Here' className=""/>
                        </div>
                        <span className={`post_in ${errors.postedIn}`}>post in #
                            <input type='text' name='postedIn'
                                   value={data.postedIn}
                                   onChange={this.onChange}
                                   placeholder='learnSomethingNew'
                            />
                        </span>
                        <textarea name='description' className={errors.description} value={data.description}
                                  onChange={this.onChange}
                                  placeholder='Description...'/>
                    </div>
                    <div className='modal_buttons'>
                        <button type='button' className={question} onClick={() => this.setState({
                            ...this.state,
                            question: 'active',
                            file: '',
                            link: '',
                            data: {...this.state.data, type: 'question'}
                        })}>
                            <img src={require('../images/icons/ques.svg')} alt=""/>
                        </button>
                        <button type='button' className={file} onClick={() => this.setState({
                            ...this.state,
                            question: '',
                            file: 'active',
                            link: '',
                            data: {...this.state.data, type: 'file'}
                        })}>
                            <img src={require('../images/icons/image.svg')} alt=""/>
                        </button>
                        <button type='button' className={link} onClick={() => this.setState({
                            ...this.state,
                            question: '',
                            file: '',
                            link: 'active',
                            data: {...this.state.data, type: 'link'}
                        })}>
                            <img src={require('../images/icons/ic_link.svg')} alt=""/>
                        </button>
                        <button type='button'><img src={require('../images/icons/pdf.svg')} alt=""/></button>
                        <button type='button'><img src={require('../images/icons/calendar.svg')} alt=""/></button>
                        <button type='button'><img src={require('../images/icons/poll.svg')} alt=""/></button>
                        {!loading && <button type='submit' className='btn_post'><span>Post </span><img
                            src={require('../images/icons/submit.svg')} alt=""/></button>}
                        {loading &&
                        <img className="editorLoading" src={require('../images/icons/editorLoading.svg')} alt=""/>}
                    </div>
                </form>
            </div>
        );
    }
}

Editor.PropTypes = {
    createPosts: PropTypes.func.isRequired
};

export default connect(null, {createPosts})(Editor);