import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import homePage from './components/pages/homePage';
import dashboard from './components/pages/dashboard';
import UserRoute from './components/routes/UserRoute';


const App = ({ location }) => (
    <div>
        <Route location={location} path="/" exact component={homePage} />
        <UserRoute location={location} path="/dashboard" exact component={dashboard} />
    </div>
);

App.propTypes = {
  location: PropTypes.shape({
      pathname: PropTypes.string.isRequired
  }).isRequired
};

export default App;
